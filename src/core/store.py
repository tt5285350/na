import asyncio
import base64
import hashlib
import pickle
import time
import typing

import redis
from Cryptodome.Cipher import AES


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Store(metaclass=Singleton):
    def __init__(self, **kwargs):
        self.start_time = time.time()

        if v := kwargs.get("redis_host"):
            redis_host = v
        else:
            redis_host = 'redis'

        if v := kwargs.get("redis_port"):
            redis_port = v
        else:
            redis_port = 6379

        self.cache = redis.Redis(host=redis_host, port=redis_port, db=0)
        self.cache.flushall()

        self.receive_queue = asyncio.Queue()
        self.producer_queue = asyncio.Queue()

        self.connect_urls: typing.List[str] = kwargs.get("connect_urls")
        self.connect_token: str = kwargs.get("connect_token")
        self.debug = kwargs.get("debug", False)
        self.environment = kwargs.get("environment", "production")

        if not self.connect_urls or not self.connect_token:
            raise ValueError(f"connect_url {self.connect_urls} and connect_token {self.connect_token} required!")

    def update(self, **kwargs):
        for k, v in kwargs:
            if hasattr(self, k) and v:
                setattr(self, k, v)

    def get_cache(self, key: str):
        if value := self.cache.get(key):
            return pickle.loads(value)
        raise ValueError(f"Object {key} are not exists in cache.")

    def set_cache(self, key, value, ex=None):
        return self.cache.set(key, pickle.dumps(value), ex)


class CryptoCipher:
    """
    https://stackoverflow.com/questions/12524994/encrypt-and-decrypt-using-pycrypto-aes-256
    """
    block_size: int = AES.block_size

    def __init__(self, key, **kwargs):
        if block_size := kwargs.get("block_size"):
            self.block_size = block_size
        self._key: bytes = hashlib.sha256(self.str_to_bytes(key)).digest()

    def get_unpad(self, s):
        return s[:-ord(s[len(s) - 1:])]

    def get_pad(self, s):
        return s + (self.block_size - len(s) % self.block_size) * self.str_to_bytes(
            chr(self.block_size - len(s) % self.block_size))

    def str_to_bytes(self, v: bytes | str) -> bytes:
        if isinstance(v, str):
            return v.encode()
        return v

    def decrypt_from_b64(self, ciphertext: bytes | str, to_string: bool = False) -> bytes | str:
        ciphertext: bytes = self.str_to_bytes(base64.b64decode(ciphertext))
        iv: bytes = ciphertext[:AES.block_size]
        cipher: AES = AES.new(self._key, AES.MODE_CBC, iv)
        t: bytes = self.get_unpad(cipher.decrypt(ciphertext[AES.block_size:]))
        if to_string:
            return t.decode()
        else:
            return t
